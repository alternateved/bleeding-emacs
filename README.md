# Unstable builds of GNU Emacs

## Description

This package is mostly aligned with the GNU Emacs package from official Fedora repositories from https://src.fedoraproject.org/rpms/emacs

## Credits & Licensing
The repository [https://gitlab.com/alternateved/bleeding-emacs](https://gitlab.com/alternateved/bleeding-emacs containing build recipes for [bleeding-emacs COPR](https://copr.fedorainfracloud.org/coprs/alternateved/bleeding-emacs) is a fork of [https://src.fedoraproject.org/rpms/emacs](https://src.fedoraproject.org/rpms/emacs).

This fork as well as the original repository ([according to FPCA](https://fedoraproject.org/wiki/Legal:Fedora_Project_Contributor_Agreement#Other_FAQs)) are licensed under MIT License. See [LICENSE](https://gitlab.com/alternateved/bleeding-emacs/-/blob/master/LICENSE) for the full license text.

This fork was also inspired by the work done in https://gitlab.com/bhavin192/emacs-pretest-rpm/ and https://gitlab.com/stephanos_komnenos/emacs-pgtk-nativecomp-copr repositories. 

## Reporting issues
If you face any issues while installing or updating, please create an issue on [GitLab repository here](https://gitlab.com/alternateved/bleeding-emacs).
